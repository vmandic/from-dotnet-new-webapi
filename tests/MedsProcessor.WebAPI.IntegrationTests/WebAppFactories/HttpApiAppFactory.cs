﻿using System;
using System.IO;
using MedsProcessor.WebAPI.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Serilog;

namespace MedsProcessor.WebAPI.IntegrationTests.Fixtures
{
	public class HttpApiAppFactory : WebApplicationFactory<Startup>
	{
		public HttpApiAppFactory()
		{
			Log.Logger = SerilogFactory.CreateDefaultLogger();
		}

		protected override void ConfigureWebHost(IWebHostBuilder builder)
		{
			Log.ForContext<HttpApiAppFactory>().Information("[INTEGRATION TESTS START] Configuring web application factory for Startup.cs");

			builder.UseEnvironment("IntegrationTests");
			builder.UseContentRoot(AppContext.BaseDirectory);
			builder.UseWebRoot(Path.Combine(AppContext.BaseDirectory, "wwwroot"));

			SetupConfiguration(builder);

			base.ConfigureWebHost(builder);
		}

		private void SetupConfiguration(IWebHostBuilder builder)
		{
			var configBuilder = new ConfigurationBuilder()
				.SetBasePath(AppContext.BaseDirectory)
				.AddJsonFile("appsettings.IntegrationTests.json")
				.AddEnvironmentVariables();

			var config = configBuilder.Build();

			builder.ConfigureAppConfiguration(opts => opts.AddConfiguration(config));
		}

	}
}