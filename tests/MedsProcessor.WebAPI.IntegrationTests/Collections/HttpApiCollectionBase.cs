﻿using System.Net.Http;
using MedsProcessor.WebAPI.Extensions;
using MedsProcessor.WebAPI.IntegrationTests.Fixtures;
using Xunit.Abstractions;

namespace MedsProcessor.WebAPI.IntegrationTests.Collections
{
		public abstract class HttpApiCollectionBase
	{
		protected HttpClient HttpApi;

		public HttpApiCollectionBase(
			HttpApiAppFactory appFactory,
			ITestOutputHelper outputHelper)
		{
			Program.XUnitOutputHelper = outputHelper;
			HttpApi = appFactory.CreateClient();
			appFactory.Server.Host.PreStartLogicAsync().GetAwaiter().GetResult();
		}
	}
}