using Xunit;

namespace MedsProcessor.WebAPI.IntegrationTests.Fixtures
{
	[CollectionDefinition(nameof(HttpApiReadOnlyCollectionFixtureBase))]
	public class HttpApiReadOnlyCollectionFixtureBase : ICollectionFixture<HttpApiAppFactory>
	{
		// ref: https://xunit.net/docs/shared-context#collection-fixture

		// This class has no code, and is never created. Its purpose is simply
		// to be the place to apply [CollectionDefinition] and all the
		// ICollectionFixture<> interfaces.
	}
}
