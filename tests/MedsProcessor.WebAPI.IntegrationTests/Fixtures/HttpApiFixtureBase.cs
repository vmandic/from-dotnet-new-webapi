using System.Net.Http;
using MedsProcessor.WebAPI.Extensions;
using Xunit;
using Xunit.Abstractions;

namespace MedsProcessor.WebAPI.IntegrationTests.Fixtures
{
		public class HttpApiFixtureBase : IClassFixture<HttpApiAppFactory>
	{
		protected readonly HttpClient HttpApi;

		public HttpApiFixtureBase(
			HttpApiAppFactory appFactory,
			ITestOutputHelper outputHelper)
		{
			Program.XUnitOutputHelper = outputHelper;
			HttpApi = appFactory.CreateClient();
			appFactory.Server.Host.PreStartLogicAsync().GetAwaiter().GetResult();
		}
	}
}