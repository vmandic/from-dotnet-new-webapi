using System.Net.Http;
using System.Net.Http.Headers;
using MedsProcessor.WebAPI.Core;
using MedsProcessor.WebAPI.IntegrationTests.Collections;
using MedsProcessor.WebAPI.IntegrationTests.Fixtures;
using MedsProcessor.WebAPI.Models;
using Newtonsoft.Json;
using Xbehave;
using Xunit;
using Xunit.Abstractions;
using Xunit.Extensions.Ordering;

namespace MedsProcessor.WebAPI.IntegrationTests.HttpRestApi.Scenarios
{
		[Order(3), Collection(nameof(HttpApiReadOnlyCollectionFixtureBase))]
	public class ProcessorTestScenarios : HttpApiCollectionBase
	{
		public class TokenResponse
		{
			[JsonProperty("access_token")]
			public string AccessToken { get; set; }
		}

		public ProcessorTestScenarios(ITestOutputHelper outputHelper, HttpApiAppFactory apiAppFactory) : base(apiAppFactory, outputHelper) { }

		[Scenario]
		[Example("admin@admin.com", "Password321$")]
		public void Should_login_and_ping_processor_status(string username, string password, HttpResponseMessage httpResponse, string token)
		{
			$"Given the username '{username}' and password '{password}'".x(() => { });

			"The user should successfully request a valid authentication JWT".x(async() =>
			{
				var requestModel = new AuthTokenRequest
				{
					Username = username,
						Password = password
				};

				httpResponse = await HttpApi.PostAsJsonAsync("/api/auth/token", requestModel);
				httpResponse.EnsureSuccessStatusCode();

				var tokenResponse = await httpResponse.Content.ReadAsAsync<ApiDataResponse<TokenResponse>>();

				Assert.NotNull(tokenResponse);
				Assert.NotNull(tokenResponse.Data);
				Assert.NotNull(tokenResponse.Data.AccessToken);
				Assert.Equal("Access token issued successfully.", tokenResponse.Message);

				// NOTE: propagate to next scenario step
				token = tokenResponse.Data.AccessToken;
			});

			"Afterwards with JWT included the user should successfully request a processor status".x(async() =>
			{
				HttpApi.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
				httpResponse = await HttpApi.GetAsync("/api/v1/processor/status");
				httpResponse.EnsureSuccessStatusCode();

				var processorResponse = await httpResponse.Content.ReadAsAsync<ApiDataResponse<HzzoDataProcessorStatus>>();

				Assert.NotNull(processorResponse);
				Assert.NotNull(processorResponse.Data);
			});
		}
	}
}