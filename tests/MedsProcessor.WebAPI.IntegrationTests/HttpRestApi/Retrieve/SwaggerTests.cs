using System.Threading.Tasks;
using MedsProcessor.WebAPI.IntegrationTests.Collections;
using MedsProcessor.WebAPI.IntegrationTests.Fixtures;
using Xunit;
using Xunit.Abstractions;
using Xunit.Extensions.Ordering;

namespace MedsProcessor.WebAPI.IntegrationTests.HttpRestApi.Retrieve
{
		[Order(1), Collection(nameof(HttpApiReadOnlyCollectionFixtureBase))]
	public class SwaggerTests : HttpApiCollectionBase
	{
		public SwaggerTests(
			HttpApiAppFactory apiAppFactory,
			ITestOutputHelper outputHelper) : base(apiAppFactory, outputHelper) { }

		[Fact, Order(1)]
		public async Task Should_open_swagger_index_html()
		{
			// ACT
			var response = await HttpApi.GetAsync("/swagger/index.html").ConfigureAwait(false);

			// ASSERT
			response.EnsureSuccessStatusCode();
			Assert.StartsWith("text/html",
				response.Content.Headers.ContentType.ToString());
		}
	}
}