using System.Threading.Tasks;
using MedsProcessor.WebAPI.IntegrationTests.Collections;
using MedsProcessor.WebAPI.IntegrationTests.Fixtures;
using Xunit;
using Xunit.Abstractions;
using Xunit.Extensions.Ordering;

namespace MedsProcessor.WebAPI.IntegrationTests.HttpRestApi.Retrieve
{
		[Order(2), Collection(nameof(HttpApiReadOnlyCollectionFixtureBase))]
	public class HomeControllerTests : HttpApiCollectionBase
	{
		public HomeControllerTests(
				ITestOutputHelper outputHelper,
				HttpApiAppFactory apiAppFactory) : base(apiAppFactory, outputHelper) { }

		[Fact]
		public async Task Should_return_200_for_homepage()
		{
			// ACT
			var response = await HttpApi.GetAsync("/").ConfigureAwait(false);

			// ASSERT
			response.EnsureSuccessStatusCode();
			Assert.StartsWith(
				"application/json",
				response.Content.Headers.ContentType.ToString());
		}
	}
}