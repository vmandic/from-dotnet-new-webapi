using Microsoft.AspNetCore.Identity;

namespace MedsProcessor.Db.Models
{
    public class AppRole : IdentityRole<int> { }
}