# Enable docker mssql 2017 support

- have Docker engine 1.8+ installed: https://docs.docker.com/engine/installation/

- `sudo docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=<YourStrong@Passw0rd>" -p 1433:1433 --name mssql -d mcr.microsoft.com/mssql/server:2017-latest`

- to start the container next time: `docker start mssql`

Read the full how-to docs: https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-docker?view=sql-server-2017
