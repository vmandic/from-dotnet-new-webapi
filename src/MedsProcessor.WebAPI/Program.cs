﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using MedsProcessor.WebAPI.Infrastructure;
using MedsProcessor.WebAPI.Extensions;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Serilog;
using Xunit.Abstractions;

namespace MedsProcessor.WebAPI
{
	public class Program
	{
		public static ITestOutputHelper XUnitOutputHelper { get; set; }

		public static readonly string ApplicationNameAndVersion =
			$"Wopis.Notifications.HttpApi-v{ Assembly.GetExecutingAssembly().GetName().Version }-{ DateTime.UtcNow.ToString("yyyyMMdd-HHmm") }";

		public static async Task Main(string[] args)
		{
			try
			{
				Log.Logger = SerilogFactory.CreateDefaultLogger();

				Log.ForContext<Program>().Information("[APP START] Configuring and building web host");
				var host = CreateWebHostBuilder(args).Build();

				await host.PreStartLogicAsync();
				await host.RunAsync();
			}
			catch (Exception ex)
			{
				Log.ForContext<Program>().Fatal(ex, "Application web host startup error");
				throw;
			}
			finally
			{
				Log.CloseAndFlush();
			}

		}

		private static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
			WebHost
			.CreateDefaultBuilder(args)
			.UseSerilogConfigured()
			.UseStartup<Startup>();
	}
}