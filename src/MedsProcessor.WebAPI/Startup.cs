using AspNetCoreRateLimit;
using MedsProcessor.Db.EntityFramework;
using MedsProcessor.Db.Models;
using MedsProcessor.Scraper;
using MedsProcessor.WebAPI.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace MedsProcessor.WebAPI
{
	public class Startup
	{
		private readonly ILogger<Startup> _logger;

		public Startup(IConfiguration configuration, ILogger<Startup> logger)
		{
			Configuration = configuration;
			_logger = logger;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			var conStr = Configuration.GetConnectionString("default");
			_logger.LogInformation("Connection string: {conStr}", conStr);

			services.AddOptions();
			services.AddMemoryCache();
			services.AddHttpContextAccessor();
			services.AddHttpClient();
			services.AddAngleSharp();
			services.AddResponseCompression(opts => opts.EnableForHttps = true);
			services.AddHealthChecks();

			services.AddDbContext<AppDbContext>(opts =>
				opts.UseSqlServer(conStr));

			services.AddIdentity<AppUser, AppRole>()
				.AddEntityFrameworkStores<AppDbContext>()
				.AddDefaultTokenProviders();

			services.ConfigureApiVersioning();
			services.ConfigureHttpRequestThrottlingByIp(Configuration);
			services.ConfigureCoreDependencies();
			services.ConfigureJwtAuthentication(Configuration);
			services.ConfigureMvcAndJsonSerializer();
			services.ConfigureSwagger();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsProduction())
				app.UseHsts();

			app.UseHttpsRedirection();

			// Expose the API for outer domain requests
			app.UseCors(opts =>
				opts.AllowAnyOrigin().AllowAnyHeader().WithMethods("GET", "POST", "OPTIONS"));

			if (!env.IsIntegrationTests())
				app.UseIpRateLimiting();

			// Handles exceptions and generates a custom response body
			app.UseExceptionHandler("/api/error");

			app.UseAuthentication();

			// Authorize access to the Swagger documentation site for non local requests
			app.UseBasicAuthentication(opts =>
			{
				opts.AuthorizeLocalRequest = false;
				opts.AuthorizeRoutes("/swagger");
				opts.Username = "admin";
				opts.Password = "admin";
			});

			app.UseResponseCompression();

			// Enable middleware to serve generated Swagger as a JSON endpoint.
			app.UseSwagger();

			// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
			app.UseSwaggerUI(c =>
				c.SwaggerEndpoint("/swagger/v1-0/swagger.json", "HZZO meds-processor v1.0"));

			app.UseHealthChecks("/health");

			app.UseMvc();
		}
	}
}