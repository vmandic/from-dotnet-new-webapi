using System.ComponentModel.DataAnnotations;

namespace MedsProcessor.WebAPI.Models
{
	public class AuthTokenRequest
	{
		// TODO: add validation logic... or maybe separte into two different auth scheme approaches :-)

		public string ClientId { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
	}
}