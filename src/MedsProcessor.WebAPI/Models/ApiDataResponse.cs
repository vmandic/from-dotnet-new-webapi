using System;

namespace MedsProcessor.WebAPI.Models
{
	public class ApiDataResponse<TObjectModel> : ApiMessageResponse where TObjectModel : class
	{
		public TObjectModel Data { get; set; }

		public ApiDataResponse(TObjectModel model, string message = null) : base(message)
		{
			Data = model;
		}
	}
}