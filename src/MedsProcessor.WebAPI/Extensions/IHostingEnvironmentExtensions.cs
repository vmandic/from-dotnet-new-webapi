using Microsoft.AspNetCore.Hosting;

namespace MedsProcessor.WebAPI.Extensions
{
	public static class IHostingEnvironmentExtensions
	{
		public static bool IsIntegrationTests(this IHostingEnvironment env)
		{
			return env.IsEnvironment("IntegrationTests");
		}
	}
}