using System.Threading.Tasks;
using AspNetCoreRateLimit;
using MedsProcessor.Db.EntityFramework;
using MedsProcessor.Db.Models;
using MedsProcessor.WebAPI.Core;
using MedsProcessor.WebAPI.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace MedsProcessor.WebAPI.Extensions
{
		public static class IWebHostExtensions
	{
		public static async Task PreStartLogicAsync(this IWebHost host)
		{
			// NOTE: handle web request scoped services
			using(var scope = host.Services.CreateScope())
			{
				var services = scope.ServiceProvider;

				// ref: https://github.com/stefanprodan/AspNetCoreRateLimit/wiki/Version-3.0.0-Breaking-Changes
				// get the IpPolicyStore instance
				var ipPolicyStore = services.GetRequiredService<IIpPolicyStore>();

				// seed IP data from appsettings
				await ipPolicyStore.SeedAsync();

				// update the database
				var db = services.GetRequiredService<AppDbContext>();
				await db.Database.MigrateAsync();

				// seed the database
				var userManager = services.GetRequiredService<UserManager<AppUser>>();
				await SeedManager.SeedDatabaseAsync(userManager);
			}

			var processor = host.Services.GetService<HzzoDataProcessor>();

#pragma warning disable
			// no need to wait for processor to finish
			processor.RunAsync();
#pragma warning restore
		}
	}
}