using MedsProcessor.WebAPI.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Serilog;

namespace MedsProcessor.WebAPI.Extensions
{
	public static class IWebHostBuilderExtensions
	{
		public static IWebHostBuilder UseSerilogConfigured(this IWebHostBuilder webHostBuilder)
		{
			return webHostBuilder.UseSerilog((webHostBuilderCtx, logConfig) =>
			{
				Log.Information("Adding enriched configuration to Serilog");
				SerilogFactory.CreateEnrichedConfiguration(
						logConfig,
						webHostBuilderCtx.Configuration,
						webHostBuilderCtx.HostingEnvironment.EnvironmentName);
			}, preserveStaticLogger : true);
		}
	}
}