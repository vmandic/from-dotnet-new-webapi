using System;
using System.Linq;
using System.Threading.Tasks;
using MedsProcessor.Db.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace MedsProcessor.WebAPI.Infrastructure
{
	public static class SeedManager
	{
		public static async Task SeedDatabaseAsync(UserManager<AppUser> userManager)
		{
			if (!await userManager.Users.AnyAsync(x => x.Email == "admin@admin.com"))
			{
				var result = await userManager.CreateAsync(new AppUser
				{
					Email = "admin@admin.com",
					EmailConfirmed = true,
					NormalizedEmail = "ADMIN@ADMIN.COM.",
					NormalizedUserName = "ADMIN@ADMIN.COM",
					UserName = "admin@admin.com",
					FirstName = "Testfirstname",
					LastName = "Testsurname"
				}, "Password321$");

				if (!result.Succeeded)
				{
					throw new InvalidOperationException(
						"Seed fail: " + string.Join(",", result.Errors.Select(x => x.Description)));
				}
			}

		}
	}
}