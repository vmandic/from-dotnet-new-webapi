using System;
using System.Reflection;
using MedsProcessor.Common;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using Serilog.Exceptions;
using Serilog.Sinks.RollingFileAlternate;
using Serilog.Sinks.SystemConsole.Themes;

namespace MedsProcessor.WebAPI.Infrastructure
{
	/// <summary>
	/// Serilog logger and configuration factory.
	/// </summary>
	/// <remarks>
	/// GUIDE: Learn about Serilog configuration: https://github.com/serilog/serilog/wiki/Configuration-Basics
	/// </remarks>
	public static class SerilogFactory
	{
		private const string SERILOG_ROLLING_FILE_DIR = "wwwroot/logs";
		private const LogEventLevel SERILOG_ROLLING_FILE_MIN_LEVEL = LogEventLevel.Debug;

		private const string SERILOG_ROLLING_FILE_OUTPUT =
			"({EnvironmentUserName} on {MachineName} in {APP} of THREAD: {ThreadId}) {Timestamp:yy-MM-dd HH:mm:ss.fff} [{Level:u3}] [SRC: {SourceContext}{Scope}] {Message:lj} {Exception}PROPS: {Properties:j}{NewLine}";

		private const string SERILOG_CONSOLE_OUTPUT =
			"{Timestamp:HH:mm:ss.fff} [{Level:u3}] {SourceContext} {Scope}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}";

		/// <summary>
		/// Creates a default configured (<see cref="SerilogFactory.CreateDefaultConfiguration" />) Serilog logger instance.
		/// </summary>
		public static Serilog.ILogger CreateDefaultLogger() =>
			CreateDefaultConfiguration().CreateLogger();

		/// <summary>
		/// If not provided a base config builds upon <see cref="SerilogFactory.CreateDefaultConfiguration" />.
		/// Enriches with exception details, thread ID, environment details, app name and adds rolling file output groupped by date.
		/// </summary>
		public static LoggerConfiguration CreateEnrichedConfiguration(
			LoggerConfiguration logConfig,
			IConfiguration appConfig,
			string environmentName)
		{
			if (appConfig is null)
			{
				throw new ArgumentNullException(nameof(appConfig));
			}

			var assemblyName = Assembly.GetExecutingAssembly().GetName();

			var defaultConfig = CreateDefaultConfiguration(logConfig)
				// GUIDE: Learn to specify configuration: https://github.com/serilog/serilog-settings-configuration
				.ReadFrom.Configuration(appConfig)
				.Enrich.WithExceptionDetails()
				.Enrich.WithProperty("APP", $"{assemblyName.Name}, v{assemblyName.Version}")
				.Enrich.WithProperty("OS", Environment.OSVersion)
				.Enrich.WithProperty("CLR", Environment.Version)
				.Enrich.WithProperty(".NETCOREENV", environmentName)
				.Enrich.WithThreadId()
				.Enrich.WithMachineName()
				.Enrich.WithEnvironmentUserName()
				// NOTE: unblock application process from I/O, ref: https://github.com/serilog/serilog-sinks-async
				.WriteTo.Async(c => c.RollingFileAlternate(
					SERILOG_ROLLING_FILE_DIR,
					logFilePrefix : Program.ApplicationNameAndVersion,
					SERILOG_ROLLING_FILE_MIN_LEVEL,
					SERILOG_ROLLING_FILE_OUTPUT));

			if (environmentName == Constants.EnvironmentNames.INTEGRATION_TESTS && Program.XUnitOutputHelper != null)
			{
				// NOTE: an additional output writer for TestResults.xml xunit test output
				// TIP: be sure to use '--logger "xunit;LogFileName=TestResults.xml"' when running tests
				defaultConfig.WriteTo.TestOutput(
					Program.XUnitOutputHelper,
					LogEventLevel.Debug,
					outputTemplate: SERILOG_CONSOLE_OUTPUT);
			}

			return defaultConfig;
		}

		/// <summary>
		/// Adds debug and console output with minimum Debug level overriding Microsoft.* to Information level.
		/// </summary>
		private static LoggerConfiguration CreateDefaultConfiguration(LoggerConfiguration logConfig = null) =>
			(logConfig ?? new LoggerConfiguration())
			.MinimumLevel.Debug()
			.MinimumLevel.Override("Microsoft", LogEventLevel.Information)
			.Enrich.FromLogContext()
			.WriteTo.Debug()
			.WriteTo.Console(outputTemplate: SERILOG_CONSOLE_OUTPUT, theme: AnsiConsoleTheme.Code);
	}
}
