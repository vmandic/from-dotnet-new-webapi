using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using MedsProcessor.Db.Models;
using MedsProcessor.WebAPI.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

namespace MedsProcessor.WebAPI.Infrastructure
{
	public interface IJwtAuthService
	{
		Task <string> IssueTokenAsync(AuthTokenRequest tokenRequest);
	}

	public class JwtAuthService : IJwtAuthService
	{
		private const string DEFAULT_CLIENT_ID = "default";
		private readonly AuthTokenOptions _tokenOpts;
		private readonly SignInManager<AppUser> _signInManager;

		public JwtAuthService(
			IOptions<AuthTokenOptions> tokenOpts,
			SignInManager<AppUser> signInManager)
		{
			this._tokenOpts = tokenOpts.Value;
			this._signInManager = signInManager;
		}

		public async Task<string> IssueTokenAsync(AuthTokenRequest tokenRequest)
		{
			if (tokenRequest == null)
				throw new ArgumentNullException(nameof(tokenRequest));

			if (!await LoginValidAsync(tokenRequest))
			{
				return null;
			}

			var jwtToken = new JwtSecurityToken(
				_tokenOpts.Issuer,
				_tokenOpts.Audience,
				new []
				{
					new Claim(JwtRegisteredClaimNames.Sub, tokenRequest.ClientId ?? tokenRequest.Username),
						new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
						new Claim(JwtRegisteredClaimNames.Iat, _tokenOpts.IssuedAtAsUnixEpoch.ToString(), ClaimValueTypes.Integer64),
				},
				_tokenOpts.NotBefore,
				_tokenOpts.Expiration,
				_tokenOpts.SigningCredentials
			);

			return new JwtSecurityTokenHandler().WriteToken(jwtToken);
		}

		private async Task<bool> LoginValidAsync(AuthTokenRequest tokenRequest)
		{
			if (tokenRequest.ClientId != DEFAULT_CLIENT_ID)
			{
				if (tokenRequest.Username is null && tokenRequest.Password is null)
				{
					return false;
				}

				var signInResult = await _signInManager.PasswordSignInAsync(
					tokenRequest.Username,
					tokenRequest.Password,
					false,
					false);

				return signInResult.Succeeded;
			}

			return true;
		}
	}
}